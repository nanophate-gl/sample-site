FROM circleci/ruby:2.7.4-node-browsers
RUN mkdir /home/circleci/project
COPY . /home/circleci/project
WORKDIR /home/circleci/project
RUN bundle install
EXPOSE 9292

# RUN rackup config.ru
