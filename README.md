# sample-site

1. To run this server run the following command to build the image.
    ```
    docker build -t rack-server .
    ```

1. Run the following command to spinup the rack server
    ```
    docker run --rm -it -p 9292:9292 rack-server rackup -o 0.0.0.0 
    ```

1. acess localhost:9292
